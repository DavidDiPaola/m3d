/*
2018,2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#ifndef __M3D_H
#define __M3D_H

/* 4x4 matrix */
struct m3d_mat4f {
	float values[4*4];
};

/* 3 number vector */
struct m3d_vec3f {
	float x;
	float y;
	float z;
};

/* 4 number vector */
struct m3d_vec4f {
	float x;
	float y;
	float z;
	float w;
};

/* quaternion */
struct m3d_quat {
	float angle;
	struct m3d_vec3f axis;
};



/* calculate camera matrix */
void
m3d_lookAt(
	const struct m3d_vec3f * eye, const struct m3d_vec3f * center, const struct m3d_vec3f * up,
	struct m3d_mat4f * out_result
);

/* calculate camera matrix */
void
m3d_perspective(
	float aspectRatio, float fovy, float zNear, float zFar,
	struct m3d_mat4f * out_result
);



/* determines if two floats are approximately equal */
int
m3d_float_isApproxEqual(float a, float b);

/* clamps a float between two values */
float
m3d_float_clamp(float value, float min, float max);



/* sets a mat4f to the identity matrix */
void
m3d_mat4f_identity(struct m3d_mat4f * out_matrix);

/* determines if two mat4f matrixes are approximately equal */
int
m3d_mat4f_isApproxEqual(const struct m3d_mat4f * a, const struct m3d_mat4f * b);

/* multiply two mat4f matrixes together */
void
m3d_mat4f_multiply(
	const struct m3d_mat4f * a, const struct m3d_mat4f * b,
	struct m3d_mat4f * out_result
);

/* multiply a mat4f and a vec4f together */
void
m3d_mat4f_multiply_vec4f(
	const struct m3d_mat4f * m, const struct m3d_vec4f * v,
	struct m3d_vec4f * out_result
);

/* multiply a mat4f and a vec3f together */
void
m3d_mat4f_multiply_vec3f(
	const struct m3d_mat4f * m, const struct m3d_vec3f * v,
	struct m3d_vec3f * out_result
);

/* apply rotation to a mat4f via a quat */
void
m3d_mat4f_rotate(
	const struct m3d_mat4f * matrix, const struct m3d_quat * rotation,
	struct m3d_mat4f * out_result
);

/* apply scaling to a mat4f via a vec3f */
void
m3d_mat4f_scale(
	const struct m3d_mat4f * matrix, const struct m3d_vec3f * scale,
	struct m3d_mat4f * out_result
);

/* apply translation to a mat4f via a vec3f */
void
m3d_mat4f_translate(
	const struct m3d_mat4f * matrix, const struct m3d_vec3f * translation,
	struct m3d_mat4f * out_result
);



/* compute the cross product of two vec3f vectors */
void
m3d_vec3f_cross(
	const struct m3d_vec3f * a, const struct m3d_vec3f * b,
	struct m3d_vec3f * out_result
);

/* compute the dot product of two vec3f vectors */
void
m3d_vec3f_dot(
	const struct m3d_vec3f * a, const struct m3d_vec3f * b,
	float * out_result
);

/* determine if two vec3f vectors are approximately equal */
int
m3d_vec3f_isApproxEqual(const struct m3d_vec3f * a, const struct m3d_vec3f * b);

/* compute the magnitude of a vec3f */
float
m3d_vec3f_magnitude(const struct m3d_vec3f * vector);

/* compute the normalized vector of a vec3f */
int
m3d_vec3f_normal(
	const struct m3d_vec3f * vector,
	struct m3d_vec3f * out_result
);



/* determine if two vec4f vectors are approximately equal */
int
m3d_vec4f_isApproxEqual(const struct m3d_vec4f * a, const struct m3d_vec4f * b);

#endif
