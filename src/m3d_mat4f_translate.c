/*
2018,2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include "m3d.h"

void
m3d_mat4f_translate(
	const struct m3d_mat4f * matrix, const struct m3d_vec3f * translation,
	struct m3d_mat4f * out_result
) {
	/* see OpenGL 2.1 glTranslate(): https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/glTranslate.xml */
	/* note: glTranslate() was deprecated, then removed from later OpenGL versions. don't use it */

	struct m3d_mat4f translation_matrix = { .values = {
		1.0f, 0.0f, 0.0f, translation->x,
		0.0f, 1.0f, 0.0f, translation->y,
		0.0f, 0.0f, 1.0f, translation->z,
		0.0f, 0.0f, 0.0f,           1.0f,
	}};
	m3d_mat4f_multiply(matrix, &translation_matrix, out_result);
}
