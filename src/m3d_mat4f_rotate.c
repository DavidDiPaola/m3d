/*
2018,2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#define _ISOC99_SOURCE
#include <math.h>

#include "m3d.h"

void
m3d_mat4f_rotate(
	const struct m3d_mat4f * matrix, const struct m3d_quat * rotation,
	struct m3d_mat4f * out_result
) {
	/* see OpenGL 2.1 glRotate(): https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/glRotate.xml */

	struct m3d_vec3f axis;
	m3d_vec3f_normal(&(rotation->axis), &axis);
	float x = rotation->axis.x;
	float y = rotation->axis.y;
	float z = rotation->axis.z;
	float c = cosf(rotation->angle);
	float s = sinf(rotation->angle);
	struct m3d_mat4f rotation_matrix = { .values = {
		(x*x*(1-c))+(  c), (x*y*(1-c))-(z*s), (x*z*(1-c))+(y*s), 0.0f,
		(y*x*(1-c))+(z*s), (y*y*(1-c))+(  c), (y*z*(1-c))-(x*s), 0.0f,
		(x*z*(1-c))-(y*s), (y*z*(1-c))+(x*s), (z*z*(1-c))+(  c), 0.0f,
		             0.0f,              0.0f,              0.0f, 1.0f,
	}};
	m3d_mat4f_multiply(matrix, &rotation_matrix, out_result);
}
