/*
2018,2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <math.h>

#include <float.h>

int
m3d_float_isApproxEqual(
	float a, float b
) {
	return (fabsf(a - b) < 0.000001f);
}
