/*
2018,2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include "m3d.h"

int
m3d_vec3f_normal(
	const struct m3d_vec3f * vector,
	struct m3d_vec3f * out_result
) {
	float vector_magnitude = m3d_vec3f_magnitude(vector);
	if (m3d_float_isApproxEqual(vector_magnitude, 0.0f)) {
		return -1;
	}

	(*out_result) = (struct m3d_vec3f){
		.x = vector->x / vector_magnitude,
		.y = vector->y / vector_magnitude,
		.z = vector->z / vector_magnitude
	};
	return 0;
}
