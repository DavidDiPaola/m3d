/*
2018,2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include "m3d.h"

void
m3d_mat4f_scale(
	const struct m3d_mat4f * matrix, const struct m3d_vec3f * scale,
	struct m3d_mat4f * out_result
) {
	/* see OpenGL 2.1 glScale(): https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/glScale.xml */

	struct m3d_mat4f scale_matrix = { .values = {
		scale->x, 0.0f,     0.0f,     0.0f,
		0.0f,     scale->y, 0.0f,     0.0f,
		0.0f,     0.0f,     scale->z, 0.0f,
		0.0f,     0.0f,     0.0f,     1.0f,
	}};
	m3d_mat4f_multiply(matrix, &scale_matrix, out_result);
}
