/*
2018,2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include "m3d.h"

int
m3d_mat4f_isApproxEqual(const struct m3d_mat4f * a, const struct m3d_mat4f * b) {
	int result = 1;

	for (int i=0; i<16; i++) {
		result &= m3d_float_isApproxEqual(a->values[i], b->values[i]);
	}

	return result;
}
