/*
2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include "m3d.h"

void
m3d_mat4f_multiply_vec3f(
	const struct m3d_mat4f * m, const struct m3d_vec3f * v,
	struct m3d_vec3f * out_result
) {
	struct m3d_vec4f v4 = {
		.x = v->x,
		.y = v->y,
		.z = v->z,
		.w = 1.0f
	};
	struct m3d_vec4f result;
	m3d_mat4f_multiply_vec4f(m, &v4, &result);

	*out_result = (struct m3d_vec3f) {
		.x = result.x,
		.y = result.y,
		.z = result.z
	};
}
