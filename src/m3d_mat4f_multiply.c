/*
2018,2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include "m3d.h"

void
m3d_mat4f_multiply(
	const struct m3d_mat4f * a, const struct m3d_mat4f * b,
	struct m3d_mat4f * out_result
) {
	float
		a_1_1 = a->values[ 0], a_1_2 = a->values[ 1], a_1_3 = a->values[ 2], a_1_4 = a->values[ 3],
		a_2_1 = a->values[ 4], a_2_2 = a->values[ 5], a_2_3 = a->values[ 6], a_2_4 = a->values[ 7],
		a_3_1 = a->values[ 8], a_3_2 = a->values[ 9], a_3_3 = a->values[10], a_3_4 = a->values[11],
		a_4_1 = a->values[12], a_4_2 = a->values[13], a_4_3 = a->values[14], a_4_4 = a->values[15]
	;

	float
		b_1_1 = b->values[ 0], b_1_2 = b->values[ 1], b_1_3 = b->values[ 2], b_1_4 = b->values[ 3],
		b_2_1 = b->values[ 4], b_2_2 = b->values[ 5], b_2_3 = b->values[ 6], b_2_4 = b->values[ 7],
		b_3_1 = b->values[ 8], b_3_2 = b->values[ 9], b_3_3 = b->values[10], b_3_4 = b->values[11],
		b_4_1 = b->values[12], b_4_2 = b->values[13], b_4_3 = b->values[14], b_4_4 = b->values[15]
	;

	/* from https://en.wikipedia.org/wiki/Matrix_multiplication#Definition */
	float * values = out_result->values;
	values[ 0] = (a_1_1*b_1_1) + (a_1_2*b_2_1) + (a_1_3*b_3_1) + (a_1_4*b_4_1);
	values[ 1] = (a_1_1*b_1_2) + (a_1_2*b_2_2) + (a_1_3*b_3_2) + (a_1_4*b_4_2);
	values[ 2] = (a_1_1*b_1_3) + (a_1_2*b_2_3) + (a_1_3*b_3_3) + (a_1_4*b_4_3);
	values[ 3] = (a_1_1*b_1_4) + (a_1_2*b_2_4) + (a_1_3*b_3_4) + (a_1_4*b_4_4);
	values[ 4] = (a_2_1*b_1_1) + (a_2_2*b_2_1) + (a_2_3*b_3_1) + (a_2_4*b_4_1);
	values[ 5] = (a_2_1*b_1_2) + (a_2_2*b_2_2) + (a_2_3*b_3_2) + (a_2_4*b_4_2);
	values[ 6] = (a_2_1*b_1_3) + (a_2_2*b_2_3) + (a_2_3*b_3_3) + (a_2_4*b_4_3);
	values[ 7] = (a_2_1*b_1_4) + (a_2_2*b_2_4) + (a_2_3*b_3_4) + (a_2_4*b_4_4);
	values[ 8] = (a_3_1*b_1_1) + (a_3_2*b_2_1) + (a_3_3*b_3_1) + (a_3_4*b_4_1);
	values[ 9] = (a_3_1*b_1_2) + (a_3_2*b_2_2) + (a_3_3*b_3_2) + (a_3_4*b_4_2);
	values[10] = (a_3_1*b_1_3) + (a_3_2*b_2_3) + (a_3_3*b_3_3) + (a_3_4*b_4_3);
	values[11] = (a_3_1*b_1_4) + (a_3_2*b_2_4) + (a_3_3*b_3_4) + (a_3_4*b_4_4);
	values[12] = (a_4_1*b_1_1) + (a_4_2*b_2_1) + (a_4_3*b_3_1) + (a_4_4*b_4_1);
	values[13] = (a_4_1*b_1_2) + (a_4_2*b_2_2) + (a_4_3*b_3_2) + (a_4_4*b_4_2);
	values[14] = (a_4_1*b_1_3) + (a_4_2*b_2_3) + (a_4_3*b_3_3) + (a_4_4*b_4_3);
	values[15] = (a_4_1*b_1_4) + (a_4_2*b_2_4) + (a_4_3*b_3_4) + (a_4_4*b_4_4);
}
