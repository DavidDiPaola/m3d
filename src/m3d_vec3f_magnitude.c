/*
2018,2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <math.h>

#include "m3d.h"

float
m3d_vec3f_magnitude(const struct m3d_vec3f * vector) {
	return sqrtf((vector->x*vector->x) + (vector->y*vector->y) + (vector->z*vector->z));
}
