/*
2018,2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include "m3d.h"

void
m3d_vec3f_dot(
	const struct m3d_vec3f * a, const struct m3d_vec3f * b,
	float * out_result
) {
	/* from Wikipedia (https://en.wikipedia.org/wiki/Dot_product#Algebraic_definition) */
	(*out_result) = (a->x*b->x) + (a->y*b->y) + (a->z*b->z);
}
