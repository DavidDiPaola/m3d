/*
2018,2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include "m3d.h"

void
m3d_lookAt(
	const struct m3d_vec3f * eye, const struct m3d_vec3f * center, const struct m3d_vec3f * up,
	struct m3d_mat4f * out_result
) {
	/* equivelant to gluLookAt() followed by glTranslatef(). see: https://www.opengl.org/discussion_boards/showthread.php/130409-using-gluLookAt-properly */
	/* see OpenGL 2.1 gluLookAt(): https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/gluLookAt.xml */
	/* see OpenGL 2.1 glTranslate(): https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/glTranslate.xml */

	struct m3d_vec3f f = {
		.x = center->x - eye->x,
		.y = center->y - eye->y,
		.z = center->z - eye->z,
	};
	struct m3d_vec3f f_norm;
	m3d_vec3f_normal(&f, &f_norm);

	struct m3d_vec3f up_norm;
	m3d_vec3f_normal(up, &up_norm);

	struct m3d_vec3f s;
	m3d_vec3f_cross(&f_norm, &up_norm, &s);

	struct m3d_vec3f s_norm;
	m3d_vec3f_normal(&s, &s_norm);

	struct m3d_vec3f u;
	m3d_vec3f_cross(&s_norm, &f_norm, &u);

	float eye_translate_x;
	m3d_vec3f_dot(&s_norm, eye, &eye_translate_x);
	eye_translate_x *= -1.0f;

	float eye_translate_y;
	m3d_vec3f_dot(&u, eye, &eye_translate_y);
	eye_translate_y *= -1.0f;

	float eye_translate_z;
	m3d_vec3f_dot(&f_norm, eye, &eye_translate_z);

	float * values = out_result->values;
	values[ 0] = s.x;          values[ 1] = s.y;          values[ 2] = s.z;          values[ 3] = eye_translate_x;
	values[ 4] = u.x;          values[ 5] = u.y;          values[ 6] = u.z;          values[ 7] = eye_translate_y;
	values[ 8] = -f_norm.x;    values[ 9] = -f_norm.y;    values[10] = -f_norm.z;    values[11] = eye_translate_z;
	values[12] = 0.0f;         values[13] = 0.0f;         values[14] = 0.0f;         values[15] = 1.0f;
}
