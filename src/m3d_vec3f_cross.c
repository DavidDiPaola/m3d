/*
2018,2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include "m3d.h"

void
m3d_vec3f_cross(
	const struct m3d_vec3f * a, const struct m3d_vec3f * b,
	struct m3d_vec3f * out_result
) {
	/* performance optimization: copy struct values into local vars */
	float a_x = a->x, a_y = a->y, a_z = a->z;
	float b_x = b->x, b_y = b->y, b_z = b->z;

	/* from Wikipedia (https://en.wikipedia.org/wiki/Cross_product#Coordinate_notation) */
	(*out_result) = (struct m3d_vec3f){
		.x = (a_y*b_z) - (a_z*b_y),
		.y = (a_z*b_x) - (a_x*b_z),
		.z = (a_x*b_y) - (a_y*b_x)
	};
}
