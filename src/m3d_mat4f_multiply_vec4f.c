/*
2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include "m3d.h"

void
m3d_mat4f_multiply_vec4f(
	const struct m3d_mat4f * m, const struct m3d_vec4f * v,
	struct m3d_vec4f * out_result
) {
	float
		m_1_1 = m->values[ 0], m_1_2 = m->values[ 1], m_1_3 = m->values[ 2], m_1_4 = m->values[ 3],
		m_2_1 = m->values[ 4], m_2_2 = m->values[ 5], m_2_3 = m->values[ 6], m_2_4 = m->values[ 7],
		m_3_1 = m->values[ 8], m_3_2 = m->values[ 9], m_3_3 = m->values[10], m_3_4 = m->values[11],
		m_4_1 = m->values[12], m_4_2 = m->values[13], m_4_3 = m->values[14], m_4_4 = m->values[15]
	;

	float
		v_x = v->x,
		v_y = v->y,
		v_z = v->z,
		v_w = v->w
	;

	/* from https://en.wikipedia.org/wiki/Matrix_multiplication#Linear_maps */
	*out_result = (struct m3d_vec4f) {
		.x = (v_x*m_1_1) + (v_y*m_1_2) + (v_z*m_1_3) + (v_w*m_1_4),
		.y = (v_x*m_2_1) + (v_y*m_2_2) + (v_z*m_2_3) + (v_w*m_2_4),
		.z = (v_x*m_3_1) + (v_y*m_3_2) + (v_z*m_3_3) + (v_w*m_3_4),
		.w = (v_x*m_4_1) + (v_y*m_4_2) + (v_z*m_4_3) + (v_w*m_4_4)
	};
}
