/*
2018,2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <math.h>

#include "m3d.h"

void
m3d_perspective(
	float aspectRatio, float fovy, float zNear, float zFar,
	struct m3d_mat4f * out_result
) {
	/* from https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/gluPerspective.xml */

	if (fovy < 0.0f) {
		return;
	}
	if (aspectRatio < 0.0f) {
		return;
	}
	if (zNear < 0.0f) {
		return;
	}
	if (zFar < 0.0f) {
		return;
	}
	if (zNear > zFar) {
		return;
	}

	float f = 1.0f / tanf(fovy / 2.0f);
	
	float * values = out_result->values;
	values[ 0] = f / aspectRatio;    values[ 1] = 0.0f;    values[ 2] =  0.0f;                          values[ 3] = 0.0f;
	values[ 4] = 0.0f;               values[ 5] = f;       values[ 6] =  0.0f;                          values[ 7] = 0.0f;
	values[ 8] = 0.0f;               values[ 9] = 0.0f;    values[10] = (zFar+zNear) / (zNear-zFar);    values[11] = (2*zFar*zNear) / (zNear-zFar);
	values[12] = 0.0f;               values[13] = 0.0f;    values[14] = -1.0f;                          values[15] = 0.0f;
}
