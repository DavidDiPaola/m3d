/*
2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include "m3d.h"

int
m3d_vec4f_isApproxEqual(const struct m3d_vec4f * a, const struct m3d_vec4f * b) {
	return (
		m3d_float_isApproxEqual(a->x, b->x)
		&&
		m3d_float_isApproxEqual(a->y, b->y)
		&&
		m3d_float_isApproxEqual(a->z, b->z)
		&&
		m3d_float_isApproxEqual(a->w, b->w)
	);
}
