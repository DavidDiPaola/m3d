/*
2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <stdio.h>

#include "../src/m3d.h"

#include "test.h"

struct _test_values {
	const char * desc;
	struct m3d_mat4f m;
	struct m3d_vec3f v;
	struct m3d_vec3f correct;
};

static int fail = 0;

static void
_test(const struct _test_values * values) {
	printf("[TEST]");

	struct m3d_vec3f result;
	m3d_mat4f_multiply_vec3f(&values->m, &values->v, &result);

	int pass = m3d_vec3f_isApproxEqual(&values->correct, &result);
	if (pass) {
		printf("[ OK ] (%s)" "\n", values->desc);
	}
	else {
		fail = 1;

		printf("[FAIL] (%s)" "\n", values->desc);

		test_mat4f_print(&values->m, "\t", NULL);

		printf("\t" "*" "\n");

		printf("\t");
		test_vec3f_print(&values->v);
		printf("\n");

		printf("\t" "was" "\n");

		printf("\t");
		test_vec3f_print(&result);
		printf("\n");

		printf("\t" "should be" "\n");

		printf("\t");
		test_vec3f_print(&values->correct);
		printf("\n");
	}
}

int
main() {
	struct _test_values testvalues[] = {
		{
			.desc="mat4f multiply vec3f test 1",
			.m={.values={
				  0.0f,   1.0f,   2.0f,   3.0f,
				  4.0f,   5.0f,   6.0f,   7.0f,
				  8.0f,   9.0f,  10.0f,  11.0f,
				 12.0f,  13.0f,  14.0f,  15.0f,
			}},
			.v={ .x=1.0f, .y=2.0f, .z=3.0f },
			.correct={ .x=11.0f, .y=39.0f, .z=67.0f },
		},
	};
	size_t testvalues_length = sizeof(testvalues) / sizeof(*testvalues);
	for (size_t i=0; i<testvalues_length; i++) {
		_test(&testvalues[i]);
	}

	if (fail) {
		return 1;
	}

	return 0;
}
