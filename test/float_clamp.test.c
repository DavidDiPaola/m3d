/*
2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <stdio.h>

#include "../src/m3d.h"

#include "test.h"

struct _test_values {
	const char * desc;
	float value;
	float min;
	float max;
	float correct;
};

static int fail = 0;

static void
_test(struct _test_values * values) {
	printf("[TEST]");

	float result = m3d_float_clamp(values->value, values->min, values->max);

	int pass = m3d_float_isApproxEqual(values->correct, result);
	if (pass) {
		printf("[ OK ] (%s)" "\n", values->desc);
	}
	else {
		fail = 1;

		printf("[FAIL] (%s)" "\n", values->desc);

		printf("\t" "clamp ");
		test_float_print(values->value);
		printf(" between ");
		test_float_print(values->min);
		printf(" and ");
		test_float_print(values->max);
		printf("\n");

		printf("\t" "was ");
		test_float_print(result);
		printf("\n");

		printf("\t" "should be ");
		test_float_print(values->correct);
		printf("\n");
	}
}

int
main() {
	struct _test_values testvalues[] = {
		{ .desc="float clamp test 1", .value=-1.0f, .min=0.0f, .max=1.0f, .correct=0.0f },
		{ .desc="float clamp test 2", .value=-0.0000001f, .min=0.0f, .max=1.0f, .correct=0.0f },
		{ .desc="float clamp test 3", .value=0.0f, .min=0.0f, .max=1.0f, .correct=0.0f },
		{ .desc="float clamp test 4", .value=0.0000001f, .min=0.0f, .max=1.0f, .correct=0.0000001f },
		{ .desc="float clamp test 5", .value=0.5f, .min=0.0f, .max=1.0f, .correct=0.5f },
		{ .desc="float clamp test 6", .value=0.9999999f, .min=0.0f, .max=1.0f, .correct=0.9999999f },
		{ .desc="float clamp test 7", .value=1.0f, .min=0.0f, .max=1.0f, .correct=1.0f },
		{ .desc="float clamp test 8", .value=1.0000001f, .min=0.0f, .max=1.0f, .correct=1.0f },
		{ .desc="float clamp test 9", .value=4.0f, .min=0.0f, .max=1.0f, .correct=1.0f },
	};
	size_t testvalues_length = sizeof(testvalues) / sizeof(*testvalues);
	for (size_t i=0; i<testvalues_length; i++) {
		_test(&testvalues[i]);
	}

	if (fail) {
		return 1;
	}

	return 0;
}

