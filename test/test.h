/*
2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#ifndef __TEST_H
#define __TEST_H

#include "../src/m3d.h"

void
test_float_print(float f);

void
test_mat4f_print(
	const struct m3d_mat4f * matrix,
	const char * line_prefix, const char * line_suffix
);

void
test_vec3f_print(const struct m3d_vec3f * vector);

void
test_vec4f_print(const struct m3d_vec4f * vector);

#endif
