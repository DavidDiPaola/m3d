/*
2018,2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <stdio.h>

#include "../src/m3d.h"

#include "test.h"

struct _test_values {
	const char * desc;
	struct m3d_vec3f a;
	float correct;
};

static int fail = 0;

static void
_test(struct _test_values values) {
	printf("[TEST]");

	float result = m3d_vec3f_magnitude(&values.a);
	if (m3d_float_isApproxEqual(result, values.correct)) {
		printf("[ OK ] (%s)" "\n", values.desc);
	}
	else {
		fail = 1;

		printf("[FAIL] (%s)" "\n", values.desc);

		printf("\t" "| ");
		test_vec3f_print(&values.a);
		printf(" |" "\n");

		printf("\t" "was" "\n");

		printf("\t");
		test_float_print(result);
		printf("\n");

		printf("\t" "should be" "\n");

		printf("\t");
		test_float_print(values.correct);
		printf("\n");
	}
}

int
main() {
	struct _test_values testvalues[] = {
		{ .desc="vec3f magnitude test 1", .a={.x=2.0f, .y=4.0f, .z=-2.0f}, .correct=4.89898f },
	};
	size_t testvalues_length = sizeof(testvalues) / sizeof(*testvalues);
	for (size_t i=0; i<testvalues_length; i++) {
		_test(testvalues[i]);
	}

	if (fail) {
		return 1;
	}

	return 0;
}

