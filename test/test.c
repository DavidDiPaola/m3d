/*
2021 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <stdio.h>

#include <math.h>

#include "../src/m3d.h"

void
test_float_print(float f) {
	printf("%g", f);

	long int rounded = lroundf(f);
	if (m3d_float_isApproxEqual((float)rounded, f)) {
		printf(".0");
	}

	printf("f");
}

void
test_mat4f_print(
	const struct m3d_mat4f * matrix,
	const char * line_prefix, const char * line_suffix
) {
	line_prefix = !line_prefix ? "" : line_prefix;
	line_suffix = !line_suffix ? "" : line_suffix;

	printf("%s" "{" "%s" "\n", line_prefix, line_suffix);
	for (int i=0; i<16; i++) {
		if ((i%4) == 0) {
			printf("%s" "\t", line_prefix);
		}
		test_float_print(matrix->values[i]);
		printf(", ");
		if ((i%4) == 3) {
			printf("%s" "\n", line_suffix);
		}
	}
	printf("%s" "}" "%s" "\n", line_prefix, line_suffix);
}

void
test_vec3f_print(const struct m3d_vec3f * vector) {
	printf("{ ");

	printf(".x=");
	test_float_print(vector->x);
	printf(", ");

	printf(".y=");
	test_float_print(vector->y);
	printf(", ");

	printf(".z=");
	test_float_print(vector->z);

	printf(" }");
}

void
test_vec4f_print(const struct m3d_vec4f * vector) {
	printf("{ ");

	printf(".x=");
	test_float_print(vector->x);
	printf(", ");

	printf(".y=");
	test_float_print(vector->y);
	printf(", ");

	printf(".z=");
	test_float_print(vector->z);
	printf(", ");

	printf(".w=");
	test_float_print(vector->w);

	printf(" }");
}
