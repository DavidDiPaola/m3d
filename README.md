# m3d
3D math routines

## dependencies

- C standard library math functions (use `-lm` flag when linking this library)

## functions

- helper functions
  - `m3d_lookAt()` -- calculates viewing matrix (like OpenGL's `gluLookAt()` and `glTranslatef()`)
  - `m3d_perspective()` -- calculates perspective matrix (like OpenGL's `gluPerspective()`)
- float functions
  - `m3d_float_isApproxEqual()` -- determines if two floats are approximately equal
  - `m3d_float_clamp()` -- clamps a float between two values
- 4x4 matrix functions
  - `m3d_mat4f_identity()` -- sets a matrix's values to the identity matrix
  - `m3d_mat4f_isApproxEqual()` -- determines if two matrixes are approximately equal
  - `m3d_mat4f_multiply()` -- multiply two matrixes together
  - `m3d_mat4f_multiply_vec3f()` -- multiply a matrix and a vector together
  - `m3d_mat4f_multiply_vec4f()` -- multiply a matrix and a vector together
  - `m3d_mat4f_rotate()` -- apply rotation to a matrix via a quaternion
  - `m3d_mat4f_scale()` -- apply scaling to a matrix via a vector
  - `m3d_mat4f_translate()` -- apply translation to a matrix via a vector
- vector functions
  - `m3d_vec3f_cross()` -- compute the cross product of two vectors
  - `m3d_vec3f_dot()` -- compute the dot product of two vectors
  - `m3d_vec3f_isApproxEqual()` -- determine if two vectors are approximately equal
  - `m3d_vec4f_isApproxEqual()` -- determine if two vectors are approximately equal
  - `m3d_vec3f_magnitude()` -- compute the magnitude of a vector
  - `m3d_vec3f_normal()` -- normalize a vector

## misc

- matrixes are stored row-major, so they will need to be converted to column-major for usage in OpenGL
