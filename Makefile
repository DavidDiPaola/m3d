BUILDDIR = build
SRC = \
	src/m3d_lookAt.c src/m3d_perspective.c \
	src/m3d_float_isApproxEqual.c src/m3d_float_clamp.c \
	src/m3d_mat4f_identity.c src/m3d_mat4f_isApproxEqual.c src/m3d_mat4f_multiply.c src/m3d_mat4f_multiply_vec4f.c src/m3d_mat4f_multiply_vec3f.c src/m3d_mat4f_rotate.c src/m3d_mat4f_scale.c src/m3d_mat4f_translate.c \
	src/m3d_vec3f_cross.c src/m3d_vec3f_dot.c src/m3d_vec3f_isApproxEqual.c src/m3d_vec3f_magnitude.c src/m3d_vec3f_normal.c \
	src/m3d_vec4f_isApproxEqual.c
A = $(BUILDDIR)/m3d.a

TEST = \
	test/float_clamp.test \
	test/mat4f_multiply.test test/mat4f_multiply_vec4f.test test/mat4f_multiply_vec3f.test \
	test/vec3f_cross.test test/vec3f_magnitude.test test/vec3f_normal.test

CFLAGS ?= \
	-std=c99 \
	-Wall -fwrapv \
	-g
LDFLAGS ?= \
	-O1
LDFLAGS_LIB = \
	-lm

OBJ = $(SRC:.c=.o)

.PHONY: all
all: build

.PHONY: build
build: src/m3d.h $(A)
	cp $< $(BUILDDIR)/

.PHONY: test
test: $(TEST)
	@for t in $^ ; do \
		./$$t ; \
	done

.PHONY: clean
clean:
	rm -f  $(OBJ)
	rm -fr $(BUILDDIR)
	rm -f  $(TEST)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

$(A): $(OBJ)
	mkdir -p $(shell dirname $(A))
	ar cvq $@ $(OBJ) > /dev/null

%.test: %.test.o test/test.o $(A)
	$(CC) $(LDFLAGS) $^ -o $@ $(LDFLAGS_LIB)
